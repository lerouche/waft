"use strict";

(undefined => {
  const BORDERABLES_SELECTOR = [
    "a",
    "button",
    "input",
    "select",
    "textarea",
  ].join(",");

  const createLine = ({fromX, fromY, toX, toY, width}) => {
    let $poly = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    let points;
    if (fromY === toY) {
      // Horizontal line
      points = [
        [fromX, fromY],
        [toX, toY],
        [toX, toY + width],
        [fromX, fromY + width]
      ];
    } else {
      // Vertical line
      points = [
        [fromX, fromY],
        [toX, toY],
        [toX + width, toY],
        [fromX + width, fromY]
      ];
    }
    $poly.setAttribute("points", points.map(p => p.join(" ")).join(" "));
    return $poly;
  };

  const createBordersSVG = () => {
    let $$borderables = [...document.querySelectorAll(BORDERABLES_SELECTOR)];

    let _ = document.createElement("div");
    _.innerHTML = `
      <svg id="borders-svg" height="100%" width="100%" xmlns="http://www.w3.org/2000/svg">
        <defs>
          <radialGradient id="circle-fill">
            <stop offset="0%" stop-color="#fff" stop-opacity="1"></stop>
            <stop offset="30%" stop-color="#fff" stop-opacity="0.9"></stop>
            <stop offset="100%" stop-color="#fff" stop-opacity="0"></stop>
          </radialGradient>
        
          <clipPath id="mask">
          </clipPath>
        </defs>
        
        <g clip-path="url(#mask)">
          <circle id="circle" cx="50%" cy="50%" r="200px"  fill="url(#circle-fill)"/>
        </g>
      </svg>
    `;
    let $svg = _.children[0];
    let svgPoint = $svg.createSVGPoint();
    let $g = $svg.children[1];
    let $mask = $svg.querySelector("#mask");
    let $circle = $svg.querySelector("#circle");

    function cursorPoint (e, svg) {
      svgPoint.x = e.clientX;
      svgPoint.y = e.clientY;
      return svgPoint.matrixTransform(svg.getScreenCTM().inverse());
    }

    function update (svgCoords) {
      $circle.setAttribute("cx", svgCoords.x);
      $circle.setAttribute("cy", svgCoords.y);
    }

    window.addEventListener("mousemove", function (e) {
      update(cursorPoint(e, $svg));
    }, false);

    for (let $bd of $$borderables) {
      const {left, top, right, bottom, x, y, width, height} = $bd.getBoundingClientRect();

      let borderWidth = 1;

      let lines = Array(4).fill(true);

      if (/^(?:a)$/i.test($bd.tagName)) {
        lines = [false, false, true, false];
      }

      lines.forEach((_, i) => {
        if (!_) {
          return;
        }

        let $line;

        switch (i) {
        case 0:
          // top
          $line = createLine({fromX: left, fromY: top, toX: left + width, toY: top, width: borderWidth});
          break;

        case 1:
          // left
          $line = createLine({fromX: left, fromY: top, toX: left, toY: top + height, width: borderWidth});
          break;

        case 2:
          // bottom
          $line = createLine({
            fromX: left,
            fromY: top + height,
            toX: left + width + borderWidth,
            toY: top + height,
            width: borderWidth
          });
          break;

        case 3:
          // right
          $line = createLine({
            fromX: left + width,
            fromY: top,
            toX: left + width,
            toY: top + height,
            width: borderWidth
          });
          break;
        }

        $mask.appendChild($line);
      });
    }

    document.body.appendChild($svg);
  };

  createBordersSVG();
})();
